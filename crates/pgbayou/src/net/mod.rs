mod client_connection;
mod net_connection;
mod server_connection;

use std::{error::Error, fmt::Display, sync::Arc, borrow::Cow};

pub use client_connection::*;
pub use net_connection::*;
pub use server_connection::*;

#[derive(Clone, Debug)]
pub enum ConnectionErrorSeverity {
    Error,
    Fatal,
    Panic,
}

impl Display for ConnectionErrorSeverity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ConnectionErrorSeverity::Error => f.write_str("error"),
            ConnectionErrorSeverity::Fatal => f.write_str("fatal"),
            ConnectionErrorSeverity::Panic => f.write_str("panic"),
        }
    }
}

impl ConnectionErrorSeverity {
    pub fn make_builder<'a>(
        self,
        code: impl Into<Cow<'a, str>>,
        message: impl Into<Cow<'a, str>>,
    ) -> ConnectionErrorBuilder<'a> {
        ConnectionErrorBuilder::new(self, code, message)
    }
}

#[derive(Clone, Debug)]
pub struct ConnectionError<'a> {
    severity: ConnectionErrorSeverity,
    code: Cow<'a, str>,
    message: Cow<'a, str>,
    detail: Option<Cow<'a,str>>,
    hint: Option<Cow<'a,str>>,
    position: Option<usize>,
    internal: Option<InternalConnectionError>,
    trace: Option<Cow<'a,str>>,
    schema: Option<Cow<'a,str>>,
    table: Option<Cow<'a,str>>,
    column: Option<Cow<'a,str>>,
    data_type: Option<Cow<'a,str>>,
    constraint: Option<Cow<'a,str>>,
    file: Option<Cow<'a,str>>,
    line: Option<usize>,
    routine: Option<Cow<'a,str>>,
}

impl<'a> Display for ConnectionError<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let severity = self.severity.clone();
        let code = self.code.clone();
        let message = self.message.clone();
        f.write_fmt(format_args!("{severity} ({code}): {message}"))
    }
}

impl<'a> Error for ConnectionError<'a> {}

pub struct ConnectionErrorBuilder<'a> {
    result: ConnectionError<'a>,
}

impl<'a> ConnectionErrorBuilder<'a> {
    pub fn new(
        severity: ConnectionErrorSeverity,
        code: impl Into<Cow<'a, str>>,
        message: impl Into<Cow<'a, str>>,
    ) -> Self {
        Self {
            result: ConnectionError {
                severity,
                code: code.into(),
                message: message.into(),
                detail: None,
                hint: None,
                position: None,
                internal: None,
                trace: None,
                schema: None,
                table: None,
                column: None,
                data_type: None,
                constraint: None,
                file: None,
                line: None,
                routine: None,
            },
        }
    }

    pub fn build(self) -> ConnectionError<'a> {
        self.result
    }

    pub fn build_err<R>(self) -> Result<R, ConnectionError<'a>> {
        Err(self.result)
    }
}

#[derive(Clone, Debug)]
pub struct InternalConnectionError {
    query: Arc<str>,
    position: usize,
}
