use zeroize::{Zeroize, ZeroizeOnDrop};

mod scram256;

#[derive(Debug)]
pub enum Error {
    InvalidResponse,
    InvalidSecret,
    InvalidSalt,
    InvalidTransition,
    IoError,
    Failure,
}

impl From<std::io::Error> for Error {
    fn from(_: std::io::Error) -> Self {
        Self::IoError
    }
}

pub enum Transition<T: std::fmt::Debug> {
    Expect(T),
    Continue(T, MechanismPayload),
    Finalize(MechanismPayload),
    Done
}

impl<T: std::fmt::Debug> std::fmt::Debug for Transition<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Expect(_) =>  write!(f, "Expect"),
            Self::Continue(_, _) =>  write!(f, "Continue"),
            Self::Finalize(_) =>  write!(f, "Finalize"),
            Self::Done => write!(f, "Done"),
        }
    }
}

fn invalid_response<T>(_: T) -> Error {
    Error::InvalidResponse
}

fn invalid_secret<T>(_: T) -> Error {
    Error::InvalidSecret
}

fn invalid_salt<T>(_: T) -> Error {
    Error::InvalidSalt
}

pub trait Mechanism : Sized + std::fmt::Debug {
    fn transition(self, incoming: Option<MechanismPayload>) -> Result<Transition<Self>, Error>;
}

#[derive(Clone, Zeroize, ZeroizeOnDrop)]
pub struct MechanismPayload(Box<[u8]>);