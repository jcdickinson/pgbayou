// TODO: This could really use some simplification, and more allocation care.

use std::io::Write;

use bytes::BufMut;
use digest::{Digest, Mac};
use hmac::Hmac;
use rand::Rng;
use sha2::Sha256;
use zeroize::{Zeroize, ZeroizeOnDrop, Zeroizing};

use super::{
    invalid_response, invalid_secret, Error, Mechanism, MechanismPayload, Transition,
};

type HmacSha256 = Hmac<Sha256>;

pub enum ClientScram256 {
    Initial {
        username: Box<[u8]>,
        secret: ClientSecret,
        nonce: Box<[u8]>,
    },
    WaitServerFirst {
        username: Box<[u8]>,
        secret: ClientSecret,
        nonce: Box<[u8]>,
    },
    WaitServerFinal {
        expected_signature: Box<[u8]>,
    },
}

impl std::fmt::Debug for ClientScram256 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Initial {
                username: _,
                secret: _,
                nonce: _,
            } => write!(f, "Initial"),
            Self::WaitServerFirst {
                username: _,
                secret: _,
                nonce: _,
            } => write!(f, "WaitServerFirst"),
            Self::WaitServerFinal {
                expected_signature: _,
            } => write!(f, "WaitServerFinal"),
        }
    }
}

impl ClientScram256 {
    pub fn new(username: impl AsRef<str>, secret: ClientSecret) -> Self {
        let nonce = rand::thread_rng()
            .sample_iter(&rand::distributions::Alphanumeric)
            .take(32)
            .collect();

        Self::Initial {
            username: username.as_ref().as_bytes().into(),
            secret,
            nonce,
        }
    }
}

impl Mechanism for ClientScram256 {
    fn transition(self, incoming: Option<MechanismPayload>) -> Result<Transition<Self>, Error> {
        match (&incoming, self) {
            (
                None,
                Self::Initial {
                    username,
                    secret,
                    nonce,
                },
            ) => {
                let initial = client_initial(&username, &nonce);
                let next = Self::WaitServerFirst {
                    username,
                    secret,
                    nonce,
                };
                Ok(Transition::Continue(next, MechanismPayload(initial)))
            }
            (
                Some(MechanismPayload(incoming)),
                Self::WaitServerFirst {
                    username,
                    secret,
                    nonce,
                },
            ) => {
                let (full_nonce, salt, rounds) = parse_initial_server(&incoming, &nonce)?;
                let client_key = secret.create_key(b"Client Key", &salt, rounds)?;
                let server_key = secret.create_key(b"Server Key", salt, rounds)?;
                let stored_key = sha256(&client_key.0);
                let signature =
                    client_signature(&username, &nonce, &incoming, full_nonce, stored_key);
                let proof = client_proof(&client_key.0, signature);
                let client_final = client_final(full_nonce, proof);
                let expected_signature =
                    client_signature(username, nonce, &incoming, full_nonce, &server_key.0);
                let next = Self::WaitServerFinal { expected_signature };
                Ok(Transition::Continue(next, MechanismPayload(client_final)))
            }
            (Some(MechanismPayload(incoming)), Self::WaitServerFinal { expected_signature }) => {
                let signature = parse_server_final(incoming)?;
                let mut buf = [0u8; 256 / 8];
                decode_base64(signature, &mut buf)?;
                if constant_time_eq::constant_time_eq(&buf, &expected_signature) {
                    Ok(Transition::Done)
                } else {
                    Err(Error::Failure)
                }
            }
            _ => Err(Error::InvalidTransition),
        }
    }
}

#[derive(Clone)]
pub enum ClientSecret {
    Password(Zeroizing<Box<[u8]>>),
    SaltedPassword {
        salt: Box<[u8]>,
        salted_password: Box<[u8]>,
        rounds: u32,
    },
}

impl ClientSecret {
    pub fn password(password: impl AsRef<str>) -> Result<Self, Error> {
        match stringprep::saslprep(password.as_ref()) {
            Ok(std::borrow::Cow::Borrowed(b)) => {
                Ok(Self::Password(Zeroizing::new(b.as_bytes().into())))
            }
            Ok(std::borrow::Cow::Owned(mut b)) => {
                let result = Ok(Self::Password(Zeroizing::new(b.as_bytes().into())));
                b.zeroize();
                result
            }
            Err(_) => Err(Error::InvalidSecret),
        }
    }

    pub fn salted_password(
        salt: impl Into<Box<[u8]>>,
        salted_password: impl Into<Box<[u8]>>,
        rounds: u32,
    ) -> Self {
        Self::SaltedPassword {
            salt: salt.into(),
            salted_password: salted_password.into(),
            rounds,
        }
    }

    fn create_key(
        &self,
        name: impl AsRef<[u8]>,
        salt: impl AsRef<[u8]>,
        rounds: u32,
    ) -> Result<PeerKey, Error> {
        match self {
            Self::SaltedPassword {
                salt: expected_salt,
                salted_password: result,
                rounds: expected_rounds,
            } => {
                if salt.as_ref() != expected_salt.as_ref() || rounds != *expected_rounds {
                    Err(Error::InvalidSalt)
                } else {
                    Ok(PeerKey(result.clone()))
                }
            }
            Self::Password(password) => {
                let mut res = [0u8; 256 / 8];
                pbkdf2::pbkdf2::<HmacSha256>(password.as_ref(), salt.as_ref(), rounds, &mut res);
                Ok(PeerKey(hmac256(res, name)))
            }
        }
    }
}

#[derive(Zeroize)]
pub enum ServerScram256<'a, T>
where
    T: ServerSecretProvider,
{
    #[zeroize(skip)]
    Initial { provider: &'a T, nonce: Box<[u8]> },
    #[zeroize(skip)]
    WaitClientFirst { provider: &'a T, nonce: Box<[u8]> },
    WaitClientFinal {
        #[zeroize(skip)]
        client_signature: HmacSha256,
        #[zeroize(skip)]
        server_signature: HmacSha256,
        stored_key: Box<[u8]>,
        nonce: Vec<u8>,
    },
}

pub trait ServerSecretProvider {
    fn get_secret(&self, username: &[u8]) -> Option<ServerSecret>;
}

impl<'a, T> ServerScram256<'a, T>
where
    T: ServerSecretProvider,
{
    pub fn new(provider: &'a T, nonce: impl AsRef<[u8]>) -> Self {
        Self::Initial {
            provider,
            nonce: nonce.as_ref().into(),
        }
    }
}

impl<'a, T> ZeroizeOnDrop for ServerScram256<'a, T> where T: ServerSecretProvider {}

impl<'a, T> std::fmt::Debug for ServerScram256<'a, T>
where
    T: ServerSecretProvider,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Initial {
                provider: _,
                nonce: _,
            } => write!(f, "Initial"),
            Self::WaitClientFirst {
                provider: _,
                nonce: _,
            } => write!(f, "WaitClientFirst"),
            Self::WaitClientFinal {
                client_signature: _,
                server_signature: _,
                stored_key: _,
                nonce: _,
            } => write!(f, "WaitClientFinal"),
        }
    }
}

impl<'a, T> Mechanism for ServerScram256<'a, T>
where
    T: ServerSecretProvider,
{
    fn transition(self, incoming: Option<MechanismPayload>) -> Result<Transition<Self>, Error> {
        match (&incoming, self) {
            (None, Self::Initial { provider, nonce }) => {
                Ok(Transition::Expect(ServerScram256::WaitClientFirst {
                    provider,
                    nonce,
                }))
            }
            (Some(MechanismPayload(payload)), Self::WaitClientFirst { provider, nonce }) => {
                let (username, client_nonce, client_initial_bare) = parse_client_initial(payload)?;
                if let Some(secret) = provider.get_secret(username) {
                    let mut full_nonce = Vec::with_capacity(client_nonce.len() + nonce.len());
                    full_nonce.extend_from_slice(client_nonce);
                    full_nonce.extend_from_slice(&nonce);

                    let mut writer = Vec::new().writer();
                    writer.write_all(b"r=")?;
                    writer.write_all(&full_nonce)?;
                    writer.write_all(b",s=")?;
                    writer.flush()?;

                    let mut writer =
                        base64::write::EncoderWriter::from(writer, &base64::engine::DEFAULT_ENGINE);
                    writer.write_all(&secret.salt)?;
                    let mut writer = writer.finish()?;

                    let rounds = secret.rounds;
                    write!(writer, ",i={rounds}")?;

                    let first_server_message = writer.into_inner();

                    let mut client_signature =
                        HmacSha256::new_from_slice(secret.stored_key.as_ref()).unwrap();
                    client_signature.update(client_initial_bare);
                    client_signature.update(b",");
                    client_signature.update(&first_server_message);
                    client_signature.update(b",");

                    let mut server_signature =
                        HmacSha256::new_from_slice(secret.server_key.as_ref()).unwrap();
                    server_signature.update(client_initial_bare);
                    server_signature.update(b",");
                    server_signature.update(&first_server_message);
                    server_signature.update(b",");

                    Ok(Transition::Continue(
                        Self::WaitClientFinal {
                            client_signature,
                            server_signature,
                            stored_key: secret.stored_key.clone(),
                            nonce: full_nonce,
                        },
                        MechanismPayload(first_server_message.into()),
                    ))
                } else {
                    // TODO: Fabricate an invalid process for invalid users
                    // * Perform all the same calculations
                    // * The salt should be deterministic
                    Err(Error::Failure)
                }
            }
            (
                Some(MechanismPayload(payload)),
                Self::WaitClientFinal {
                    mut client_signature,
                    mut server_signature,
                    stored_key,
                    nonce,
                },
            ) => {
                let (proof, bare) = parse_client_final(payload, nonce)?;
                let mut proof_bytes = [0u8; 256 / 8];
                decode_base64(proof, &mut proof_bytes)?;
                client_signature.update(bare);
                server_signature.update(bare);

                let mut writer = Vec::new().writer();
                writer.write_all(b"v=")?;
                writer.flush()?;

                let server_signature = server_signature
                    .finalize()
                    .into_bytes()
                    .into_iter()
                    .collect::<Vec<u8>>();

                let mut writer =
                    base64::write::EncoderWriter::from(writer, &base64::engine::DEFAULT_ENGINE);
                writer.write_all(&server_signature)?;
                let mut writer = writer.finish()?;
                writer.flush()?;
                let server_signature = writer.into_inner();

                let client_signature = client_signature
                    .finalize()
                    .into_bytes()
                    .into_iter()
                    .zip(proof_bytes)
                    .map(|(x, y)| x ^ y)
                    .collect::<Vec<u8>>();

                let expected_stored_key = sha256(&client_signature);
                if !constant_time_eq::constant_time_eq(&expected_stored_key, &stored_key) {
                    Err(Error::Failure)
                } else {
                    Ok(Transition::Finalize(MechanismPayload(
                        server_signature.into(),
                    )))
                }
            }
            _ => Err(Error::InvalidTransition),
        }
    }
}

#[derive(Zeroize, ZeroizeOnDrop, Clone)]
pub struct ServerSecret {
    salt: Box<[u8]>,
    server_key: Box<[u8]>,
    stored_key: Box<[u8]>,
    rounds: u32,
}

impl ServerSecret {
    pub fn new(salt: Box<[u8]>, server_key: Box<[u8]>, stored_key: Box<[u8]>, rounds: u32) -> Self {
        Self {
            salt,
            server_key,
            stored_key,
            rounds,
        }
    }

    pub fn generate(
        salt: impl AsRef<[u8]>,
        password: impl AsRef<str>,
        rounds: u32,
    ) -> Result<Self, Error> {
        zeroprep(password, |password| {
            let salt = salt.as_ref();
            let salted_password = Zeroizing::new(hi(password.as_bytes(), salt, rounds));
            let client_key = hmac256(&salted_password, b"Client Key");
            let server_key = hmac256(&salted_password, b"Server Key");
            let stored_key = sha256(client_key);
            Ok(Self {
                salt: salt.into(),
                server_key,
                stored_key,
                rounds,
            })
        })
    }
}

#[derive(Zeroize, ZeroizeOnDrop)]
struct PeerKey(Box<[u8]>);

fn zeroprep<R>(
    password: impl AsRef<str>,
    f: impl FnOnce(&str) -> Result<R, Error>,
) -> Result<R, Error> {
    let pwd = stringprep::saslprep(password.as_ref()).map_err(invalid_secret)?;
    let result = f(pwd.as_ref());
    if let std::borrow::Cow::Owned(mut owned) = pwd {
        owned.zeroize()
    }
    result
}

fn client_initial(username: impl AsRef<[u8]>, nonce: impl AsRef<[u8]>) -> Box<[u8]> {
    b"n,,n="
        .iter()
        .chain(username.as_ref())
        .chain(b",r=")
        .chain(nonce.as_ref())
        .map(|v| *v)
        .collect()
}

fn client_final(full_nonce: impl AsRef<[u8]>, proof: impl AsRef<[u8]>) -> Box<[u8]> {
    b"c=biws,r="
        .iter()
        .chain(full_nonce.as_ref())
        .chain(b",p=")
        .chain(base64::encode(proof.as_ref()).as_bytes())
        .map(|v| *v)
        .collect()
}

fn parse_initial_server<'a>(
    value: &'a [u8],
    nonce: impl AsRef<[u8]>,
) -> Result<(&'a [u8], Vec<u8>, u32), Error> {
    let nonce = nonce.as_ref();
    // 'r=' 'nonce' ',s=X' ',i=X'
    if value.len() < 2 + nonce.len() + 4 + 4 {
        return Err(Error::InvalidResponse);
    }

    if &value[0..2] != b"r=" {
        return Err(Error::InvalidResponse);
    }
    let value = &value[2..];

    let (full_nonce, value) = match value.iter().position(|&v| v == b',') {
        None | Some(0) => return Err(Error::InvalidResponse),
        Some(v) => value.split_at(v),
    };

    if !constant_time_eq::constant_time_eq(&full_nonce[0..nonce.len()], nonce) {
        return Err(Error::InvalidResponse);
    }

    // ',s=X' ',i=X'
    if value.len() < 4 + 4 || &value[0..3] != b",s=" {
        return Err(Error::InvalidResponse);
    }
    let value = &value[3..];

    let (salt, value) = match value.iter().position(|&v| v == b',') {
        None | Some(0) => return Err(Error::InvalidResponse),
        Some(v) => value.split_at(v),
    };

    // ',i=X'
    if value.len() < 4 || &value[0..3] != b",i=" {
        return Err(Error::InvalidResponse);
    }
    let value = &value[3..];

    let rounds = match value.iter().position(|&v| v == b',') {
        Some(0) => return Err(Error::InvalidResponse),
        None => value,
        Some(v) => &value[0..v],
    };

    let salt = base64::decode(std::str::from_utf8(salt).map_err(invalid_response)?)
        .map_err(invalid_response)?;
    let rounds = std::str::from_utf8(rounds)
        .map_err(invalid_response)?
        .parse::<u32>()
        .map_err(invalid_response)?;

    Ok((full_nonce, salt, rounds))
}

fn parse_client_initial<'a>(value: &'a [u8]) -> Result<(&'a [u8], &'a [u8], &'a [u8]), Error> {
    // 'n,,n=X' ',r=X'
    if value.len() < 6 + 4 {
        return Err(Error::InvalidResponse);
    }

    if &value[0..5] != b"n,,n=" {
        return Err(Error::InvalidResponse);
    }
    let client_bare = &value[3..];
    let value = &value[5..];

    let (username, value) = match value.iter().position(|&v| v == b',') {
        None | Some(0) => return Err(Error::InvalidResponse),
        Some(v) => value.split_at(v),
    };

    // ',r=X'
    if value.len() < 4 || &value[0..3] != b",r=" {
        return Err(Error::InvalidResponse);
    }
    let value = &value[3..];

    let nonce = match value.iter().position(|&v| v == b',') {
        Some(0) => return Err(Error::InvalidResponse),
        None => value,
        Some(v) => &value[0..v],
    };

    Ok((username, nonce, client_bare))
}

fn parse_server_final<'a>(value: &'a [u8]) -> Result<&'a [u8], Error> {
    // 'v='
    if value.len() < 2 {
        return Err(Error::InvalidResponse);
    }

    if &value[0..2] != b"v=" {
        return Err(Error::InvalidResponse);
    }
    let value = &value[2..];

    let proof = match value.iter().position(|&v| v == b',') {
        None => value,
        Some(0) => return Err(Error::InvalidResponse),
        Some(v) => &value[0..v],
    };

    Ok(proof)
}

fn parse_client_final<'a>(
    value: &'a [u8],
    nonce: impl AsRef<[u8]>,
) -> Result<(&'a [u8], &'a [u8]), Error> {
    let nonce = nonce.as_ref();
    let bare = value;

    // 'c=biws,r=' nonce ',p=X'
    if value.len() < 9 + nonce.len() + 4 {
        return Err(Error::InvalidResponse);
    }

    if &value[0..9] != b"c=biws,r=" {
        return Err(Error::InvalidResponse);
    }
    let value = &value[9..];

    let (client_nonce, value) = match value.iter().position(|&v| v == b',') {
        None | Some(0) => return Err(Error::InvalidResponse),
        Some(v) => value.split_at(v),
    };

    if !constant_time_eq::constant_time_eq(&client_nonce[0..nonce.len()], nonce) {
        return Err(Error::InvalidResponse);
    }

    let bare = &bare[..(9 + client_nonce.len())];

    // ',p=X'
    if value.len() < 4 || &value[0..3] != b",p=" {
        return Err(Error::InvalidResponse);
    }
    let value = &value[3..];

    let proof = match value.iter().position(|&v| v == b',') {
        Some(0) => return Err(Error::InvalidResponse),
        None => value,
        Some(v) => &value[0..v],
    };

    Ok((proof, bare))
}

fn sha256(buf: impl AsRef<[u8]>) -> Box<[u8]> {
    let mut mac = sha2::Sha256::new();
    mac.update(buf);
    mac.finalize().into_iter().collect()
}

fn hmac256(key: impl AsRef<[u8]>, data: impl AsRef<[u8]>) -> Box<[u8]> {
    let mut mac = HmacSha256::new_from_slice(key.as_ref()).unwrap();
    mac.update(data.as_ref());
    let arr = mac.finalize().into_bytes().into_iter().collect();
    arr
}

fn decode_base64(str: impl AsRef<[u8]>, dest: &mut [u8; 256 / 8]) -> Result<(), Error> {
    let str = str.as_ref();
    if str.len() != 44 {
        Err(Error::InvalidResponse)
    } else if str[str.len() - 1] != b'=' {
        Err(Error::InvalidResponse)
    } else if base64::decode_engine_slice(str, dest, &base64::engine::DEFAULT_ENGINE)
        .map_err(invalid_response)?
        == dest.len()
    {
        Ok(())
    } else {
        Err(Error::InvalidResponse)
    }
}

fn client_signature(
    username: impl AsRef<[u8]>,
    nonce: impl AsRef<[u8]>,
    server: impl AsRef<[u8]>,
    full_nonce: impl AsRef<[u8]>,
    key: impl AsRef<[u8]>,
) -> Box<[u8]> {
    let mut mac = HmacSha256::new_from_slice(key.as_ref()).unwrap();
    mac.update(b"n=");
    mac.update(username.as_ref());
    mac.update(b",r=");
    mac.update(nonce.as_ref());
    mac.update(b",");
    mac.update(server.as_ref());
    mac.update(b",");
    mac.update(b"c=biws,r=");
    mac.update(full_nonce.as_ref());

    let arr = mac.finalize().into_bytes().into_iter().collect();
    arr
}

fn client_proof(client_key: impl AsRef<[u8]>, client_signature: impl AsRef<[u8]>) -> Box<[u8]> {
    client_key
        .as_ref()
        .iter()
        .zip(client_signature.as_ref().iter())
        .map(|(x1, x2)| x1 ^ x2)
        .collect()
}

fn hi(str: impl AsRef<[u8]>, salt: impl AsRef<[u8]>, i: u32) -> Box<[u8]> {
    let mut res = [0u8; 256 / 8];
    pbkdf2::pbkdf2::<HmacSha256>(str.as_ref(), salt.as_ref(), i, &mut res);
    res.into()
}

#[cfg(test)]
mod test {
    use digest::Mac;

    use crate::sasl::{
        scram256::{ServerScram256, ServerSecret},
        Mechanism, MechanismPayload, Transition, Error,
    };

    use super::{
        client_proof, client_signature, hi, hmac256, sha256, ClientScram256, ClientSecret,
        HmacSha256, ServerSecretProvider,
    };

    struct RfcServerSecretProvider;
    impl ServerSecretProvider for RfcServerSecretProvider {
        fn get_secret(&self, username: &[u8]) -> Option<ServerSecret> {
            assert_eq!(String::from_utf8_lossy(username), "user".to_string());
            return Some(
                ServerSecret::generate(
                    base64::decode("W22ZaJ0SNY7soEsUEjb6gQ==").unwrap(),
                    "pencil",
                    4096,
                )
                .unwrap(),
            );
        }
    }

    #[test]
    pub fn rfc_test() {
        let password = "pencil";
        // "n=user,r=rOprNGfwEbeRWgbNEkqO";
        // "c=biws,r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0";

        let server_first = "r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0,s=W22ZaJ0SNY7soEsUEjb6gQ==,i=4096";

        let salt = base64::decode("W22ZaJ0SNY7soEsUEjb6gQ==").unwrap();
        let salted_password = hi(password, salt, 4096);
        let client_key = hmac256(&salted_password, b"Client Key");
        let server_key = hmac256(salted_password, b"Server Key");

        let stored_key = sha256(client_key.clone());
        let signature = client_signature(
            b"user",
            b"rOprNGfwEbeRWgbNEkqO",
            server_first,
            b"rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0",
            &stored_key,
        );

        let proof = client_proof(client_key, signature);
        let proof = base64::encode(proof);
        assert_eq!(proof, "dHzbZapWIk4jUhN+Ute9ytag9zjfMHgsqmmiz7AndVQ=");

        let server_signature = client_signature(
            b"user",
            b"rOprNGfwEbeRWgbNEkqO",
            server_first,
            b"rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0",
            server_key,
        );
        let server_signature = base64::encode(server_signature);
        assert_eq!(
            server_signature,
            "6rriTRBi23WpRR/wtup+mMhUZUn/dB5nLTJRsjl95G4="
        );
    }

    #[test]
    pub fn rfc_client_transition_first() {
        let client = ClientScram256::Initial {
            username: Box::new(*b"user"),
            secret: ClientSecret::password("pencil").unwrap(),
            nonce: Box::new(*b"rOprNGfwEbeRWgbNEkqO"),
        };
        match &client.transition(None).unwrap() {
            Transition::Continue(
                ClientScram256::WaitServerFirst {
                    username: _,
                    secret: _,
                    nonce: _,
                },
                MechanismPayload(payload),
            ) => {
                assert_eq!(
                    std::str::from_utf8(payload.as_ref()).unwrap(),
                    "n,,n=user,r=rOprNGfwEbeRWgbNEkqO"
                );
            }
            v => panic!("wanted ClientScram256::WaitServerFirst, got {v:?}"),
        };
    }

    #[test]
    pub fn rfc_client_transition_wait_server_first() {
        let incoming = MechanismPayload(Box::new(
            *b"r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0,s=W22ZaJ0SNY7soEsUEjb6gQ==,i=4096",
        ));
        let client = ClientScram256::WaitServerFirst {
            username: Box::new(*b"user"),
            secret: ClientSecret::password("pencil").unwrap(),
            nonce: Box::new(*b"rOprNGfwEbeRWgbNEkqO"),
        };
        match &client.transition(Some(incoming)).unwrap() {
            Transition::Continue(
                ClientScram256::WaitServerFinal { expected_signature },
                MechanismPayload(payload),
            ) => {
                assert_eq!(
                    std::str::from_utf8(payload.as_ref()).unwrap(),
                    "c=biws,r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0,p=dHzbZapWIk4jUhN+Ute9ytag9zjfMHgsqmmiz7AndVQ="
                );
                assert_eq!(
                    base64::encode(expected_signature),
                    "6rriTRBi23WpRR/wtup+mMhUZUn/dB5nLTJRsjl95G4="
                );
            }
            v => panic!("wanted ClientScram256::WaitServerFinal, got {v:?}"),
        };
    }

    #[test]
    pub fn rfc_client_transition_wait_server_final() {
        let incoming =
            MechanismPayload(Box::new(*b"v=6rriTRBi23WpRR/wtup+mMhUZUn/dB5nLTJRsjl95G4="));
        let client = ClientScram256::WaitServerFinal {
            expected_signature: base64::decode("6rriTRBi23WpRR/wtup+mMhUZUn/dB5nLTJRsjl95G4=")
                .unwrap()
                .into(),
        };
        match client.transition(Some(incoming)).unwrap() {
            Transition::Done => {}
            v => panic!("wanted ClientScram256::Done, got {v:?}"),
        }
    }

    #[test]
    pub fn rfc_create_client_key() {
        let salt = base64::decode("W22ZaJ0SNY7soEsUEjb6gQ==").unwrap();
        let client_secret = ClientSecret::password("pencil").unwrap();
        let client_key = client_secret.create_key(b"Client Key", salt, 4096).unwrap();
        assert_eq!(
            base64::encode(&client_key.0),
            "pg/JI9Z+hkSpLRa5btpe9GVrDHJcSEN0viVTVXaZbos="
        );
    }

    #[test]
    pub fn rfc_server_transition_initial() {
        let provider = RfcServerSecretProvider;
        let server = ServerScram256::new(&provider, b"1234");
        let result = server.transition(None).unwrap();
        assert!(matches!(result, Transition::Expect(_)))
    }

    #[test]
    pub fn rfc_server_transition_second() {
        let provider = RfcServerSecretProvider;
        let server = ServerScram256::WaitClientFirst {
            provider: &provider,
            nonce: Box::new(*b"%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0"),
        };
        let result = server
            .transition(Some(MechanismPayload(Box::new(
                *b"n,,n=user,r=rOprNGfwEbeRWgbNEkqO",
            ))))
            .unwrap();
        match &result {
            Transition::Continue(
                ServerScram256::WaitClientFinal {
                    client_signature,
                    server_signature: _,
                    stored_key: _,
                    nonce: _,
                },
                MechanismPayload(payload),
            ) => {
                assert_eq!(
                    client_signature
                        .clone()
                        .finalize()
                        .into_bytes()
                        .into_iter()
                        .collect::<Vec<u8>>(),
                    Vec::from([
                        // This was created from the correct partial signature
                        146, 23, 15, 89, 14, 74, 17, 0, 142, 25, 70, 208, 87, 160, 3, 130, 69, 5,
                        149, 214, 135, 146, 231, 91, 67, 58, 167, 190, 153, 66, 168, 19
                    ])
                );
                assert_eq!(
                    String::from_utf8_lossy(payload),
                    "r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0,s=W22ZaJ0SNY7soEsUEjb6gQ==,i=4096"
                );
            }
            _ => panic!("Expected Continue"),
        }
    }

    #[test]
    pub fn rfc_server_transition_final() {
        let provider = RfcServerSecretProvider;
        let cli = provider.get_secret(b"user").unwrap();

        let mut client_signature = HmacSha256::new_from_slice(&cli.stored_key).unwrap();
        client_signature.update(b"n=user,r=rOprNGfwEbeRWgbNEkqO,r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0,s=W22ZaJ0SNY7soEsUEjb6gQ==,i=4096,");

        let mut server_signature = HmacSha256::new_from_slice(&cli.server_key).unwrap();
        server_signature.update(b"n=user,r=rOprNGfwEbeRWgbNEkqO,r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0,s=W22ZaJ0SNY7soEsUEjb6gQ==,i=4096,");

        let server = ServerScram256::WaitClientFinal::<RfcServerSecretProvider> {
            client_signature,
            server_signature,
            stored_key: cli.stored_key.clone(),
            nonce: (&b"rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0"[..]).into(),
        };

        let result = server
            .transition(Some(MechanismPayload(Box::new(
                *b"c=biws,r=rOprNGfwEbeRWgbNEkqO%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0,p=dHzbZapWIk4jUhN+Ute9ytag9zjfMHgsqmmiz7AndVQ=",
            ))))
            .unwrap();
        match &result {
            Transition::Finalize(MechanismPayload(payload)) => {
                assert_eq!(
                    String::from_utf8_lossy(payload),
                    "v=6rriTRBi23WpRR/wtup+mMhUZUn/dB5nLTJRsjl95G4="
                );
            }
            _ => panic!("Expected Continue"),
        }
    }

    #[test]
    pub fn rfc_full() {

        fn execute(a: impl Mechanism, b: impl Mechanism) -> Result<(), Error> {
            let mut b_to_a = None;
            let mut a_to_b = None;
            let mut a = Some(a);
            let mut b = Some(b);

            while a.is_some() || b.is_some() {
                if let Some(current) = b {
                    (b_to_a, b) = match current.transition(a_to_b.clone())? {
                        Transition::Expect(next) => (None, Some(next)),
                        Transition::Continue(next, payload) => {
                            (Some(payload), Some(next))
                        },
                        Transition::Finalize(payload) => {
                            (Some(payload), None)
                        },
                        Transition::Done => {
                            (None, None)
                        }
                    };
                }
                if let Some(current) = a {
                    (a_to_b, a) = match current.transition(b_to_a.clone())? {
                        Transition::Expect(next) => (None, Some(next)),
                        Transition::Continue(next, payload) => {
                            (Some(payload), Some(next))
                        },
                        Transition::Finalize(payload) => {
                            (Some(payload), None)
                        },
                        Transition::Done => {
                            (None, None)
                        }
                    };
                }
            }
            Ok(())
        }

        let provider = RfcServerSecretProvider;

        let cli_secret = ClientSecret::password("pencil").unwrap();
        let cli = ClientScram256::new("user", cli_secret);
        
        let server = ServerScram256::new(&provider, b"%hvYDpWUa2RaTCAfuxFIlj)hNlF$k0");

        execute(cli, server).unwrap();
    }
}
