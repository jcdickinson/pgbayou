use bytes::BytesMut;
use tokio::io::{AsyncRead, AsyncWrite};
use tracing::{debug, warn};

use super::{
    ConnectionErrorSeverity, MessageType, NetConnection, NetConnectionError, StartupMessageType,
};

pub struct ServerConnection<S> {
    connection: NetConnection<S>,
    write_buffer: BytesMut,
}

impl<S> From<NetConnection<S>> for ServerConnection<S> {
    fn from(connection: NetConnection<S>) -> Self {
        Self {
            connection,
            write_buffer: BytesMut::new(),
        }
    }
}

impl<S> ServerConnection<S> {
    pub fn connection(&mut self) -> &mut NetConnection<S> {
        &mut self.connection
    }
}

impl<S: AsyncRead + AsyncWrite + Unpin> ServerConnection<S> {
    pub async fn open(&mut self) -> Result<(), NetConnectionError> {
        let con = &mut self.connection;
        let buf = &mut self.write_buffer;

        con.write_transport(buf, StartupMessageType::Startup, |b| {
            b.str("user").str("postgres");
            b.str("database").str("postgres");
            b.str("client_encoding").str("UTF8");
            b.null();
            b.end();
        })
        .await?;

        let message_type = con.read_message_type(&mut None).await?;
        let limit = &mut con.read_limit(&mut None).await?;

        match message_type {
            MessageType::Authentication => {
                assert_eq!(con.read_u32(limit).await?, 10);
                let mechanism = con.read_string(limit).await?;
            }
            MessageType::Error => todo!("error"),
            MessageType::ReadyForQuery => todo!("ready"),
        }

        Ok(())
    }
}
