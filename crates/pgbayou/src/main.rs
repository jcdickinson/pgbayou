use anyhow::{anyhow, Context, Result};
use net::{ClientConnection, NetConnection, ServerConnection};
use tokio::{
    io::{self, AsyncRead, AsyncWrite},
    net::{TcpListener, TcpSocket, TcpStream},
};
use tracing::{debug, error, Level};
use tracing_subscriber::FmtSubscriber;
mod net;
#[allow(dead_code)]
pub mod sasl;

fn main() -> io::Result<()> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::TRACE)
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(async {
            let client = TcpSocket::new_v4()?;
            let stream = client.connect("127.0.0.1:5432".parse().unwrap()).await?;
            let netcon = NetConnection::new(stream);
            let mut con = ServerConnection::from(netcon);
            con.open().await.unwrap();

            Ok(())
            // let listener = TcpListener::bind("127.0.0.1:5433").await?;
            // debug!("listening...");
            // loop {
            //     let (socket, _) = listener.accept().await?;
            //     tokio::task::spawn(async move {
            //         process_socket(socket).await;
            //     });
            // }
        })
}

async fn process_socket(socket: TcpStream) {
    let con = NetConnection::new(socket);
    match handle_socket(con).await {
        Err(error) => error!("client disconnected: {}", error),
        _ => {}
    }
}

async fn handle_socket<S: AsyncRead + AsyncWrite + Unpin>(con: NetConnection<S>) -> Result<()> {
    let mut con = ClientConnection::from(con);
    con.setup().await?;

    Ok(())
}
