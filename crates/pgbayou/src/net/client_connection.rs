use bytes::BytesMut;
use tokio::io::{AsyncRead, AsyncWrite};
use tracing::debug;

use crate::net::{MessageType, StartupMessageType};

use super::{ConnectionErrorSeverity, NetConnection, NetConnectionError};

pub struct ClientConnection<S> {
    connection: NetConnection<S>,
    write_buffer: BytesMut,
}

impl<S> From<NetConnection<S>> for ClientConnection<S> {
    fn from(connection: NetConnection<S>) -> Self {
        Self {
            connection,
            write_buffer: BytesMut::new(),
        }
    }
}

impl<S> ClientConnection<S> {
    pub fn connection(&mut self) -> &mut NetConnection<S> {
        &mut self.connection
    }
}

impl<S: AsyncRead + AsyncWrite + Unpin> ClientConnection<S> {
    pub async fn setup(&mut self) -> Result<(), NetConnectionError> {
        let con = &mut self.connection;
        loop {
            let length = con.read_u32(&mut None).await? as usize;
            let limit = &mut con.read_limit(&mut None).await?;

            match con.read_startup_message_type(limit).await {
                Ok(StartupMessageType::StartTls) => con.write_u8(b'N').await?, // SSL
                Ok(StartupMessageType::Startup) => {
                    while !matches!(limit, None | Some(1)) {
                        let key = con.read_string(limit).await?;
                        let value = con.read_string(limit).await?;
                        debug!("{} = {}", key, value);
                    }

                    if con.read_u8(limit).await? != b'\0' {
                        con.write_error(
                            &mut self.write_buffer,
                            &ConnectionErrorSeverity::Fatal
                                .make_builder("08P01", "invalid startup packet")
                                .build(),
                        )
                        .await?;
                    }
                    break;
                }
                Err(NetConnectionError::UnknownStartupMessage(unknown)) => {
                    con.write_error(
                        &mut self.write_buffer,
                        &ConnectionErrorSeverity::Fatal
                            .make_builder("08P01", format!("unknown startup packet type {unknown}"))
                            .build(),
                    )
                    .await?;
                    return Err(NetConnectionError::Broken);
                }
                Err(err) => return Err(err),
            }
        }

        debug!("connected");
        con.write(&mut self.write_buffer, MessageType::Authentication, |b| {
            b.u32(3).end()
        })
        .await?;

        let msg = con.read_u8(&mut None).await?;
        let len = con.read_u32(&mut None).await? as usize;
        let mut limit = Some(len - 4);
        let str = con.read_string(&mut limit).await?;
        debug!("pass: {} {}", msg, str);

        con.write(&mut self.write_buffer, MessageType::Authentication, |b| {
            b.u32(0).end()
        })
        .await?;
        con.write(&mut self.write_buffer, MessageType::ReadyForQuery, |b| {
            b.u8(b'I').end()
        })
        .await?;

        con.write_error(
            &mut self.write_buffer,
            &ConnectionErrorSeverity::Fatal
                .make_builder("08P01", format!("WIP"))
                .build(),
        )
        .await?;

        Ok(())
    }
}
