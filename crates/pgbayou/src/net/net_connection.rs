use bytes::{Buf, BytesMut};
use std::{mem::size_of, string::FromUtf8Error};
use thiserror::Error;
use tokio::io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt};

use super::ConnectionError;

#[derive(Debug, Error)]
pub enum NetConnectionError {
    #[error("connection reset by peer")]
    ConnectionReset,
    #[error("I/O error: {0}")]
    IO(std::io::Error),
    #[error("invalid UTF8")]
    InvalidUtf8,
    #[error("connection closed")]
    Closed,
    #[error("read past limit")]
    EndOfLimit,
    #[error("unknown startup message type {0}")]
    UnknownStartupMessage(u32),
    #[error("unknown message type {0}")]
    UnknownMessage(u8),
    #[error("bad connection state")]
    Broken,
}

impl From<std::io::Error> for NetConnectionError {
    fn from(err: std::io::Error) -> Self {
        Self::IO(err)
    }
}

impl From<FromUtf8Error> for NetConnectionError {
    fn from(_: FromUtf8Error) -> Self {
        Self::InvalidUtf8
    }
}

pub enum StartupMessageType {
    StartTls,
    Startup,
}

impl From<StartupMessageType> for u32 {
    fn from(message: StartupMessageType) -> Self {
        match message {
            StartupMessageType::StartTls => 80877103,
            StartupMessageType::Startup => 196608,
        }
    }
}

impl TryFrom<u32> for StartupMessageType {
    type Error = u32;

    fn try_from(value: u32) -> Result<Self, Self::Error> {
        match value {
            80877103 => Ok(StartupMessageType::StartTls),
            196608 => Ok(StartupMessageType::Startup),
            unknown => Err(unknown),
        }
    }
}

pub enum MessageType {
    Authentication,
    Error,
    ReadyForQuery,
}

impl From<MessageType> for u8 {
    fn from(message: MessageType) -> Self {
        match message {
            MessageType::Authentication => b'R',
            MessageType::Error => b'E',
            MessageType::ReadyForQuery => b'Z',
        }
    }
}

impl TryFrom<u8> for MessageType {
    type Error = u8;

    fn try_from(value: u8) -> Result<Self, u8> {
        match value {
            b'R' => Ok(MessageType::Authentication),
            b'E' => Ok(MessageType::Error),
            b'Z' => Ok(MessageType::ReadyForQuery),
            unknown => Err(unknown),
        }
    }
}

pub struct NetConnection<S> {
    stream: S,
    buffer: BytesMut,
}

impl<S> NetConnection<S> {
    pub fn new(stream: S) -> Self {
        Self {
            stream,
            buffer: BytesMut::new(),
        }
    }
}

impl<S: AsyncRead + Unpin> NetConnection<S> {
    async fn more_bytes(&mut self) -> Result<(), NetConnectionError> {
        if self.stream.read_buf(&mut self.buffer).await? == 0 {
            if self.buffer.is_empty() {
                return Err(NetConnectionError::Closed);
            } else {
                return Err(NetConnectionError::ConnectionReset);
            }
        }
        Ok(())
    }

    async fn at_least_bytes(
        &mut self,
        len: usize,
        limit: &mut Option<usize>,
    ) -> Result<(), NetConnectionError> {
        check_limit(len, *limit)?;
        while self.buffer.remaining() < len {
            self.more_bytes().await?
        }
        Ok(())
    }

    async fn skip(
        &mut self,
        len: usize,
        limit: &mut Option<usize>,
    ) -> Result<(), NetConnectionError> {
        self.at_least_bytes(len, limit).await?;
        advance_limit(len, limit)?;
        self.buffer.advance(len);
        Ok(())
    }

    async fn fill_span(
        &mut self,
        dst: &mut [u8],
        limit: &mut Option<usize>,
    ) -> Result<(), NetConnectionError> {
        self.at_least_bytes(dst.len(), limit).await?;
        advance_limit(dst.len(), limit)?;
        self.buffer.copy_to_slice(dst);
        Ok(())
    }

    pub async fn read_string(
        &mut self,
        limit: &mut Option<usize>,
    ) -> Result<String, NetConnectionError> {
        let mut position = 0usize;
        loop {
            while position < self.buffer.remaining() {
                if self.buffer[position] == 0 {
                    let mut temp = vec![0u8; position];
                    self.buffer.copy_to_slice(temp.as_mut());
                    self.skip(1, limit).await?;

                    return Ok(String::from_utf8(temp)?);
                }
                advance_limit(1, limit)?;
                position += 1;
            }
            self.more_bytes().await?;
        }
    }

    pub async fn read_u32(&mut self, limit: &mut Option<usize>) -> Result<u32, NetConnectionError> {
        let mut buffer = [0u8; 4];
        self.fill_span(&mut buffer, limit).await?;
        Ok(u32::from_be_bytes(buffer))
    }

    pub async fn read_limit(
        &mut self,
        limit: &mut Option<usize>,
    ) -> Result<Option<usize>, NetConnectionError> {
        let result = self.read_u32(limit).await? as usize;
        if result < 4 {
            Err(NetConnectionError::EndOfLimit)
        } else {
            Ok(Some(result - 4))
        }
    }

    pub async fn read_startup_message_type(
        &mut self,
        limit: &mut Option<usize>,
    ) -> Result<StartupMessageType, NetConnectionError> {
        StartupMessageType::try_from(self.read_u32(limit).await?)
            .map_err(|u| NetConnectionError::UnknownStartupMessage(u))
    }

    pub async fn read_u8(&mut self, limit: &mut Option<usize>) -> Result<u8, NetConnectionError> {
        let mut buffer = [0u8; 1];
        self.fill_span(&mut buffer, limit).await?;
        Ok(buffer[0])
    }

    pub async fn read_message_type(
        &mut self,
        limit: &mut Option<usize>,
    ) -> Result<MessageType, NetConnectionError> {
        let mut buffer = [0u8; 1];
        self.fill_span(&mut buffer, limit).await?;
        buffer[0]
            .try_into()
            .map_err(|v| NetConnectionError::UnknownMessage(v))
    }
}

impl<S: AsyncWrite + Unpin> NetConnection<S> {
    pub async fn write_error<'a>(
        &mut self,
        buf: &mut BytesMut,
        error: &ConnectionError<'a>,
    ) -> Result<(), NetConnectionError> {
        self.write(buf, MessageType::Error, |b| {
            let s = match error.severity {
                super::ConnectionErrorSeverity::Error => "ERROR",
                super::ConnectionErrorSeverity::Fatal => "FATAL",
                super::ConnectionErrorSeverity::Panic => "PANIC",
            };
            b.u8(b'S').str(s);
            b.u8(b'V').str(s);
            b.u8(b'C').str(&error.code);
            b.u8(b'M').str(&error.message);
            b.u8(0);
            b.end();
        })
        .await?;
        Ok(())
    }

    pub async fn write_u8(&mut self, msg: u8) -> Result<(), NetConnectionError> {
        let buf = [msg; 1];
        self.stream.write_all(&buf[..]).await?;
        Ok(())
    }

    pub async fn write_transport<F>(
        &mut self,
        buf: &mut BytesMut,
        msg: StartupMessageType,
        f: F,
    ) -> Result<(), NetConnectionError>
    where
        F: FnOnce(&mut PacketBuilder),
    {
        buf.clear();
        let src = {
            let mut builder = PacketBuilder { buf };
            builder.u32(msg.into());
            f(&mut builder);
            builder
        }
        .buf;
        let len = src.remaining() + size_of::<u32>();
        let len = (len as u32).to_be_bytes();
        self.stream.write_all(&len[..]).await?;
        self.stream.write_all_buf(src).await?;
        Ok(())
    }

    pub async fn write<F>(
        &mut self,
        buf: &mut BytesMut,
        msg: MessageType,
        f: F,
    ) -> Result<(), NetConnectionError>
    where
        F: FnOnce(&mut PacketBuilder),
    {
        buf.clear();
        let src = {
            let mut builder = PacketBuilder { buf };
            f(&mut builder);
            builder
        }
        .buf;
        let len = src.remaining() + size_of::<u32>();
        let len = (len as u32).to_be_bytes();
        let mut header = [msg.into(); 5];
        header[1..].copy_from_slice(&len);
        self.stream.write_all(&header[..]).await?;
        self.stream.write_all_buf(src).await?;
        Ok(())
    }

    pub async fn copy_from<D: AsyncRead + Unpin>(
        &mut self,
        src: &mut NetConnection<D>,
        mut num: usize,
    ) -> Result<(), NetConnectionError> {
        while num > 0 {
            let remaining = match src.buffer.remaining() {
                0 => {
                    src.more_bytes().await?;
                    src.buffer.remaining()
                }
                v => v,
            };
            let to_write = &mut src.buffer.split_to(remaining.min(num));
            num -= to_write.remaining();
            self.stream.write_all_buf(to_write).await?;
        }
        Ok(())
    }
}

pub struct PacketBuilder<'a> {
    buf: &'a mut BytesMut,
}

impl<'a> PacketBuilder<'a> {
    pub fn null(&mut self) -> &mut Self {
        self.u8(0)
    }

    pub fn u8(&mut self, msg: u8) -> &mut Self {
        let b = [msg; 1];
        self.buf.extend_from_slice(&b);
        self
    }

    pub fn u32(&mut self, val: u32) -> &mut Self {
        let src = val.to_be_bytes();
        self.buf.extend_from_slice(&src);
        self
    }

    pub fn str(&mut self, val: impl AsRef<str>) -> &mut Self {
        let val = val.as_ref().as_bytes();
        self.buf.reserve(val.len() + 1);
        let nul = [0u8; 1];
        self.buf.extend_from_slice(val);
        self.buf.extend_from_slice(&nul);
        self
    }

    pub fn end(&mut self) {}
}

fn check_limit(len: usize, limit: Option<usize>) -> Result<(), NetConnectionError> {
    if let Some(limit) = limit {
        if len > limit {
            return Err(NetConnectionError::EndOfLimit);
        }
    }
    Ok(())
}

fn advance_limit(len: usize, limit: &mut Option<usize>) -> Result<(), NetConnectionError> {
    if let Some(limit) = limit {
        if len > *limit {
            return Err(NetConnectionError::EndOfLimit);
        }
        *limit -= len;
    }
    Ok(())
}
